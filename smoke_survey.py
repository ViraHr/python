# Importing libraries
import pandas as pd
import matplotlib.pyplot as plt

# Opening a file
f = open("smokers.csv")
df = pd.read_csv(f)

# Step 1
print("Step 1")
print("===========")
# Displaying the number of rows in the data
print("Number of Surveys:", len(df))

# Step 2
print("\nStep 2")
print("===========")
# Displaying number of states
all_st = df['State'].unique().tolist()
st = df.groupby("State")
print("Number of States:", len(st))
# Displaying the average number of surveys per state
surveys = df.groupby(["State"])["Value"].count()
outcome = sum(surveys)/ len(st)
print("Average number of Surveys per State:", outcome)


# Step 3
print("\nStep 3")
print("===========")
# Displaying the minimum smoking rate
minUse = df["Value"].idxmin()  
minNum = df.loc[minUse]    
print("Minimum Recorded Cigarette Use:")
print(minNum)

# Displaying the maximum smoking rate
maxUse = df["Value"].idxmax()
maxNum = df.loc[maxUse] 
print("\nMaximum Recorded Cigarette Use:")
print(maxNum)

# Step 4
print("\nStep 4")
print("===========")
result = df.groupby(["State"])["Value"].mean()
# Displaying the states with minimum average smoker rate
print("Least Cigarette Use State:")
print(result.nsmallest(1))
# Displaying the states with maximum average smoker rate
print("\nMost Cigarette Use State:")
print(result.nlargest(1))

# Step 5
print("\nStep 5")
print("===========")
# Displaying the top 10 states with average smoker rate
print("Top 10 Most Cigarette Use States:")
topUse = df.groupby("State").mean()
topUse = (topUse.sort_values("Value", ascending=False)).head(10)
print(topUse)

# Step 6
print("\nStep 6")
print("===========")
# Getting state and year from the user as inputs
state = input("State: ")
year = int(input("Enter Year: "))
# Displaying the surveys based on user input (state, year)
analysis = df[(df["State"]  == state) & (df["Year"]  == year)]
print("Found", len(analysis), "surveys")
print(analysis)

# Step 7
print("\nStep 7")
print("===========")

# Creating a function to display the average smoker rate year by year (analysis sentence and graph)
def getInfo(userInput):
    if userInput == "Exit":
        print("\nBye bye!")   
    
    elif userInput not in all_st:
        print("hmmm... cannot find the state")   
             
    elif userInput in all_st:    
        surveys = df[df["State"]== userInput]
        result = surveys.groupby(["State", "Year"])["Value"].mean()
        
        x = list(result.values)
        y = result.index
        a = []
            
        for i in y:
            a.append(i[1])
        
        plt.plot(a, x)
        plt.show() 
        
        if x[-1] < x[-2]:
            print("\nCigarette use is on decline in", userInput) 
        else:
            print("\nCigarette use is on rise in", userInput)   

# Getting state from the user as input 
userInput = input("Enter State: ")
getInfo(userInput) 

# Displaying the average smoker rate year by year by invoking the getInfo function based on the user input     
while userInput != "Exit":           
    print("Compute for another state or enter 'Exit' to exit.") 
    userInput = input("Enter State: ")
    getInfo(userInput) 
