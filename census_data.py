# Step 0 - Read the csv file and create a class.
class County:
    name = ""
    state = ""
    commute = 0
    home = 0
    income = 0
    pop = 0
    edu = 0   
 
    def isSimilarTo(self, y):
        output = sim(self.pop, y.pop) and sim(self.edu, y.edu) and sim(self.home, y.home) and sim(self.income, y.income) and sim(self.commute, y.commute) 
        return output

    def isSameAs(self, y):
        return self.name == y.name and self.state == y.state

    def label(self):
        return self.name + " (" + self.state + ")"
    
    def happiness(self):
        rate = (self.income * self.home) / self.commute
        return rate 

def sim(x, y):
        return abs(x / y - 1)< 0.02  
    
data = []

f = open("county_facts.csv") 
import csv
rows = csv.reader(f) 

next(rows)

# skipping an empty row
for cols in rows:
    if cols[2]== "":
        continue

# creating a list of county objects
    c = County()
    c.name = cols[1]
    c.state = cols[2]
    c.pop = float(cols[6])
    c.edu = float(cols[23])
    c.income = float(cols[33])
    c.home = float(cols[27])
    c.commute = float(cols[25])
    
    data.append(c)

f.close()   

# Step 1 -​ Display the least and most populous counties.
cMax = max(data, key = lambda x: x.pop)
cMin = min(data, key = lambda x: x.pop)
print("\nQ1: Counties with least and most population")
print("===========================================")
print(cMin.name, "(", cMin.state,"):", cMin.pop)
print(cMax.name, "(",cMax.state,"):", cMax.pop)

 
# Step 2 - Display the states with least and most population.
print("\nQ2: States with least and most population")
print("=========================================") 
def statePop(state, counties):
    stateCounties = filter(lambda x: x.state == state, counties)
    pops = list(map(lambda x: x.pop, stateCounties))
    output = sum(pops)
    return output
     
states = set(map(lambda x: x.state, data))
states = list(map(lambda x: {"State": x, "population": statePop(x, data)}, states))

print(min(states, key=lambda x: x["population"]))
print(max(states, key=lambda x: x["population"]))    
   
     
# Step 3 - Display a correlation between happiness and the rate of education
print("\nQ3: Hapiness vs Higher Education Rate")
print("======================================")
def average(lst):
    lst = list(lst)
    return sum(lst)/ len(lst)

#average higher education rate am​ong counties
happyRatings = map(lambda x: x.happiness(), data)
avg = average(happyRatings)
print("Average is", avg)

happyCounties = filter(lambda x: x.happiness() >= avg, data)
avgEduHappy = average(map(lambda x: x.edu, happyCounties))
print("Happy Counties College Edu Rate:", avgEduHappy, "%")

otherCounties = filter(lambda x: x.happiness() < avg, data)
avgEduOther = average(map(lambda x: x.edu, otherCounties))
print("Unhappy Counties College Edu Rate:", avgEduOther, "%")

diff = avgEduHappy - avgEduOther
print("Difference is", diff,"%")
if diff > 20:
    print("Happy counties have significantly more college graduates.")
elif diff > 5:
    print ("Happy counties have slightly more college graduates.")
elif diff > -20:
    print("Happy counties have significantly less college graduates.")
elif diff > -5:
    print ("Happy counties have slightly less college graduates.")
else:
    print("Could not find any significant correlation.")    
    
    
# Step 4 - Display all similar counties 
print("\nQ4: Similar Counties (2%)")
print("=========================")

allSimilar = set()

for c in data:
    # Remove c from data
    selfrem = filter(lambda x: not x.isSameAs(c), data)

    # Find states that are similar in terms of population and education
    similar = list(filter(lambda x: x.isSimilarTo(c), selfrem))

    for simC in similar:
        if c.pop > simC.pop:
            allSimilar.add(simC.label() + " and " + c.label() + " are similar.")
        else:
            allSimilar.add(c.label() + " and " + simC.label() + " are similar.")

#printing similar counties
for item in allSimilar:
    print(item)
 


# Step 5 - Display a correlation between population and income on a graph
print("\nQ5: Population vs Income Rate")
print("=============================")

y = []
x = []

#dividing counties into 3 equal segments
spacing = (cMax.pop - cMin.pop) / 3
for i in range(3):
    r1 = cMin.pop + spacing * i
    r2 = r1 + spacing
    fData = filter(lambda x: r1 <= x.pop <= r2, data)
    incomes = map(lambda x: x.income, fData)
    x.append(str(r1) + "-" + str(r2))
    y.append(average(incomes))
    
import matplotlib.pyplot as plt
plt.plot(x, y)
plt.show()    


    


































          