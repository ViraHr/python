f = open("contributions.csv")

import csv
reader = csv.reader(f)

next(reader) 

#creating a class
class Contribution: 
    def __init__(self, candidate, name, gender, age, donation):
        self.Candidate = candidate
        self.Name = name
        self.Gender = gender
        self.Age = age
        self.Donation = donation
    
data = []
# skipping an empty row
for row in reader:
    if row[0] == "":
        continue

    gender = row[3]
    if gender != 'male':
        gender = 'female'
                    
    c = Contribution(row[0], row[1] + " " + row[2], gender, int(row[4]), float(row[5]))
    data.append(c)
    
f.close()    

# Step 0 - Create a function to count the number of donations, total amount of donations and average amount.    
def Record(lst):
    print("\tThere are ", len(lst), " donations.")
    amount = sum(list(map(lambda x: x.Donation, lst)))
    print("\tTotal amount is $", amount) 
    print("\tAverage contribution was $", amount/len(lst))

# Step 1 - Determine how many candidates are running
candidates = list(set(map(lambda x: x.Candidate, data)))
print("STEP 1:")
print("In this election, " + str(len(candidates)) + " candidates are running:")
for i in range(len(candidates)):
    print("\tCandidate", i+ 1, ":", candidates[i])       

# Step 2 - Report contributions for each candidate
print("\nSTEP 2:")
for i in candidates:
    print("For candidate ", i, ":")
    Record(list(filter(lambda x: x.Candidate == i, data))) 

# Step 3 - Check donations made by individuals younger than 21 year old.
print("\nSTEP 3:")
print("Donations from underage donors:")
Record(list(filter(lambda x: x.Age < 21, data)))
        
# Step 4 - Check any contribution that is more than $1600 and report the names
print("\nSTEP 4:")
print("Following donors exceeded amount limit per election:")
donors = set(map(lambda x: x.Name, data))
for donor in donors:
    don = list(filter(lambda x: x.Name == donor, data)) 
    amount = sum(list(map(lambda x: x.Donation, don))) 
    if amount > 1600:
        print("\t", donor, "contributed $", amount)

# Step 5 - Report donations made by gender for each canditate
print("\nSTEP 5:")
for i in candidates:
    print("Females for candidate ", i, ":")
    Record(list(filter(lambda x: x.Candidate == i and x.Gender == "female", data)))
    
    print("Males for candidate ", i, ":")
    Record(list(filter(lambda x: x.Candidate == i and x.Gender == "male", data)))
    
    
    