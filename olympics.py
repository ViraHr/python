import pandas as pd

# load the csv file using pandas
df = pd.read_csv("athlete_events.csv")

# STEP 0
print("Step 0")
print("========")
# Display the number of records in the file
print("There are", len(df), "records in the data-set.")

# STEP 1
print("\nStep 1")
print("========")
# Change NA values in Medal column to string None
df["Medal"].fillna("None", inplace = True)

# Display the number of records that have a medal associated with them.
medals = df[df["Medal"] != "None"]
print("There are", len(medals), "records in the data-set with medals." )

# STEP 2
print("\nStep 2")
print("========")

# Display the top three sports that have the tallest (on average) athletes
tallest_players = (df.groupby("Sport")["Height"].mean()).nlargest(3)
print("Tallest in", tallest_players)

# Display the top three sports that have the shortest (on average) athletes
shortest_players = (df.groupby("Sport")["Height"].mean()).nsmallest(3)
print("\nShortest in", shortest_players)

# Display the top three sports that have the heaviest (on average) athletes
heaviest_players = (df.groupby("Sport")["Weight"].mean()).nlargest(3)
print("\nHeaviest in", heaviest_players)

# Display the top three sports that have the lightest (on average) athletes
lightest_players = (df.groupby("Sport")["Weight"].mean()).nsmallest(3)
print("\nLightest in", lightest_players)

# STEP 3
print("\nStep 3")
print("========")
# Display the team with the most Gold medals year by year for winter olympics.​
s = df[df["Season"]== "Winter"]
years = sorted(s["Year"].unique())
medal = s[s["Medal"]== "Gold"]    
print(medal.groupby("Year")["Team"].apply(lambda x: x.value_counts().head(1)))

# STEP 4
print("Step 4")
print("========")
# Display which team dominates with most medals for each sport
sport_type = sorted(df["Sport"].unique())
for sport in sport_type:
    m_m = df[(df["Sport"] == sport) & (df["Medal"] != "None")]
    print(sport, m_m.groupby("Team")["Medal"].count().nlargest(1))   
    
# STEP 5
print("Step 5")
print("========")
# Create a graph displaying the participation of female athletes on summer olympics year by year 
s_s = df[df["Season"]== "Summer"]
years = sorted(s_s["Year"].unique())
counts = []
for year in years:
    ath = s_s[s_s["Year"] == year]
    females = ath[ath["Sex"] == "F"]
    counts.append(len(females)/len(ath))
    
import matplotlib.pyplot as plt  
plt.plot(years, counts)
plt.show() 

# STEP 6
print("Step 6")  
print("========")
#Create a graph displaying average athlete age year by year.
average_age = []
for year in years:
  average_age.append(df[df["Year"] == year]["Age"].mean())
      
plt.plot(years, average_age)
plt.show()     

# STEP 7
print("Step 7") 
print("========")  
# Display the top 3 sports that is least competitive of 2016 olympics only .
print ("We recommend:")
yr = df[df["Year"] == 2016]
result = []
for sport in sport_type:
    sp = yr[yr["Sport"] == sport]
    sp_medals = sp[sp["Medal"] != "None"]
   
    if len(sp) == 0:
        continue
    
    r = len(sp_medals) / len(sp)
    result.append({"sport" : sport, "ratio" : r})
result = sorted(result, key = lambda x: x["ratio"], reverse = True)
print(result [0]['sport'])
print(result [1]['sport'])
print(result [2]['sport'])