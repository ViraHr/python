flights = []
f = open("airline_delays.csv")

content = f.read()
f.close()


rows = content.split("\n")

first_row = True

for row in rows:
    if first_row:
        first_row = False
        continue

    values = row.split(",")

    if values[0] != "" and values[2] != "" and values[4] != "":
        flights.append({"airport": values[0], "delay": float(values[2]), "distance": float(values[4])}) 


airports = {}
for rec in flights:
    name = rec["airport"]
    arr_delays = rec["delay"]
    
    if name in airports:
        airports[name].append(arr_delays)
    else:
        airports[name] = [arr_delays]

# STEP 1
airport = input("Origin Airport: ")
number_of_flights = airports[airport]

delayed_count = 0
for delay in number_of_flights:
    if delay > 0:
        delayed_count += 1
        
on_time = len(number_of_flights) - delayed_count 
   
print("Total number of flights originated from %s airport was %d." % (airport, len(number_of_flights)))  
print("Flights from %s were on time %d times." % (airport, on_time))
print("Flights from %s were delayed %d times." % (airport, delayed_count))  

rating = (len(number_of_flights) - delayed_count) / len(number_of_flights)
print ("The perfomance of flights from %s is %s." % (airport, rating))


# STEP 2
best = ""
best_rating = 0
worst = ""
worst_rating = 1

for airport in airports:
    number_of_flights = airports[airport]
    delayed_count = 0
    for delay in number_of_flights:
        if delay > 0:
            delayed_count += 1
    rating = (len(number_of_flights) - delayed_count) / len(number_of_flights) 
  
    print("The perfomance of flights from %s is %s." % (airport, rating))

    if rating > best_rating:
        best = airport
        best_rating = rating
        
    if rating < worst_rating:
        worst = airport
        worst_rating = rating
               
print("Best airport %s with perfomance %s." % (best, best_rating))       
print("Worst airport %s with perfomance %s."  %(worst, worst_rating))
      

# STEP 3
data = []

for i in range(7):
    start = i * 400 + 1
    end = i * 400 + 400
    
    total_count = 0
    one_time_count = 0
    for flight in flights:
        if flight["distance"] < start or flight["distance"] > end:
            continue
        
        if flight["delay"] == 0:
            one_time_count += 1
            
        total_count += 1
       
    rating = one_time_count / total_count
    data.append(rating)
    
import matplotlib.pyplot as plt
plt.plot(data)
plt.show()  

            
            