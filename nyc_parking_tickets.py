import pandas as pd

# load the csv file using pandas
df = pd.read_csv("nyc_parking_tickets.csv")

# STEP 1
print("Step 1: Reading file...")
print("=======================")
# Display the number of records in the file
print(len(df), "records were read from file.")

# STEP 2
# Cleaning data
print("\nStep 2: Cleaning up...")
print("=======================")

clean = df[(
    (df["Registration_State"] != "99")
    & (df["Plate_Type"] != "999")
    & (df["Vehicle_Make"] != "")
    & (df["Vehicle_Year"] != 0)
    & (df["Issuer_Code"] != 0)
    & (df["Vehicle_Year"] < 2018)
    )];

print(len(clean), "records left after cleaning.")

# STEP 3
#Create a graph displaying a number of tickets for each vehicle year
print("\nStep 3: # of tickets by vehicle year...")
print("=======================================")
years = sorted(clean["Vehicle_Year"].unique())
n_of_tickets = []
for year in years:
    n_of_tickets.append(clean[clean["Vehicle_Year"] == year]["Issuer_Code"].count())

import matplotlib.pyplot as plt  
plt.plot(years, n_of_tickets)
plt.show()

# STEP 4
# Display the top 5 vehicle-makes with most tickets
print("\nStep 4: Top 5 vehicle-makes with most tickets...")
print("================================================")
vehicle_make = (clean.groupby("Vehicle_Make")["Plate_ID"].count()).nlargest(5)
print(vehicle_make)


# STEP 5
# Display the street where commercial vehicles got the most ticket
print("\nStep 5: The street where commercial vehicles got the most ticket:")
print("=================================================================")
pl = clean[clean["Plate_Type"] == "COM"]
most_tickets = (pl.groupby("Street_Name")["Plate_ID"].count()).nlargest(1)
print(most_tickets)

# STEP 6.1
# Display the state whith average newest vehicle year
print("\nStep 6.1: The state with newest vehicles:")
print("=========================================")
new_v = clean[["Registration_State", "Vehicle_Year"]] 
newest = new_v.groupby(["Registration_State"]).mean().sort_values("Vehicle_Year", ascending = False).head(1)
print(newest)

# STEP 6.2
# Display the state with average oldest vehicle year
print("\nStep 6.2: The state with oldest vehicles:")
print("=========================================")
old_v = clean[["Registration_State", "Vehicle_Year"]] 
oldest = old_v.groupby(["Registration_State"]).mean().sort_values("Vehicle_Year", ascending = True).head(1)
print(oldest)